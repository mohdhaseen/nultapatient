package com.craterzone.nultapatient.fragment;


import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.craterzone.nultapatient.R;
import com.craterzone.nultapatient.asynctask.loginPatientTask;
import com.craterzone.nultapatient.model.request.LoginReq;

public class LoginFragment extends Fragment implements OnClickListener{
	
	private View mRootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.login_layout, container, false);
		return mRootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		mRootView.findViewById(R.id.submit).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit:
			 verifyNumber();
			break;

		default:
			break;
		}
	}
	
	private void verifyNumber() {
		LoginReq requestModel = new LoginReq();
		EditText phoneNumEditText = ((EditText) mRootView.findViewById(R.id.phone_number_edittext));
		String phoneString = phoneNumEditText.getText().toString();
		if (phoneString.length() == 0) 
		{
			Toast.makeText(getActivity(), getString(R.string.phone_validation),Toast.LENGTH_SHORT).show();
			phoneNumEditText.requestFocus();
			return;
		}
		if (phoneString.length() < 10)
		{
			Toast.makeText(getActivity(), getString(R.string.phone_validation),Toast.LENGTH_SHORT).show();
			phoneNumEditText.setText("");
			phoneNumEditText.requestFocus();
			return;
		}
		requestModel.setMobile(phoneString);
		Spinner spin = (Spinner) mRootView.findViewById(R.id.country_spinner);
		int item = spin.getSelectedItemPosition();
		requestModel.setCountry(91);
		showConfirmPhoneNoDialog(requestModel);
		
	}

	
	
	private void showConfirmPhoneNoDialog(final LoginReq requestModel) {
		
		final Dialog dialog = new Dialog(getActivity(), R.style.Theme_Dialog);
		dialog.setContentView(R.layout.confirm_phone_dialog);
		((TextView)dialog.findViewById(R.id.id_confirm_phone_no_txt_view)).setText("+ " + String.format(getResources().getString(R.string.confirm_phone_no), requestModel.getCountry(), requestModel.getMobile()));
		dialog.findViewById(R.id.id_edit_txt_view).setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						dialog.dismiss(); 
					}
				});
		
		dialog.findViewById(R.id.id_confirm_txt_view).setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
					  new loginPatientTask(getActivity(), requestModel).execute();  
	            		dialog.dismiss();
					}
				});
		dialog.show();
  }

}
