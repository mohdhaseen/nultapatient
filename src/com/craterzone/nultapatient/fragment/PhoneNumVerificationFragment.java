package com.craterzone.nultapatient.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.craterzone.cz_view_lib.CustomToast;
import com.craterzone.nultapatient.R;
import com.craterzone.nultapatient.asynctask.VerificationTask;
import com.craterzone.nultapatient.model.request.VerificationReq;
import com.craterzone.nultapatient.util.NultaSharedPreferenceUtil;

public class PhoneNumVerificationFragment extends Fragment implements OnClickListener{

	
	private View mRootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.activity_phone_verification, container, false);
		String phoneNumber =NultaSharedPreferenceUtil.getInstance(getActivity()).getPhoneNumber("");
		phoneNumber = String.format(getResources().getString(R.string.phone_verification_heading), formatPhoneNumber(phoneNumber));
		((TextView) mRootView.findViewById(R.id.phone_verification_heading)).setText(phoneNumber);
		return mRootView;
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
		mRootView.findViewById(R.id.submit_button).setOnClickListener(this);
	}


	@Override
	public void onClick(View v) {
		verify();
	}
	
	
	private void verify() {
		VerificationReq request = new VerificationReq();
		request.setUserId(NultaSharedPreferenceUtil.getInstance(getActivity()).getUserID(0));
		request.setMobileNo(NultaSharedPreferenceUtil.getInstance(getActivity()).getPhoneNumber(null));
		request.setCode(1234);
	    if(((EditText)mRootView.findViewById(R.id.phone_number)).getText().toString().equals(Integer.toString(request.getCode())))
	      {
	    	 new VerificationTask(getActivity(), request).execute(); 
	      } else {
	    	CustomToast.showShortToast(getActivity(),getString( R.string.pin_code));	
	    	((EditText)mRootView.findViewById(R.id.phone_number)).setText("");
	    	((EditText)mRootView.findViewById(R.id.phone_number)).requestFocus();
	    }
	}
	
	
	private String formatPhoneNumber(String phone) {
		char[]a =phone.toCharArray();
		char[]newArray = new char[a.length+2];
		for(int i=0,j=0;i<newArray.length;i++)
		{
			if(i==4)
			{
				newArray[i]=' ';
			}
			else if(i==8)
			{
				newArray[i]=' ';
			}
			else
			{
				newArray[i]=a[j];
				j++;
			}
		}
		return String.valueOf(newArray);
	}
	
}
