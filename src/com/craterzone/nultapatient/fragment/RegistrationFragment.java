package com.craterzone.nultapatient.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.craterzone.cz_view_lib.CustomToast;
import com.craterzone.media.images.CameraUtil;
import com.craterzone.media.images.ImageUtil;
import com.craterzone.nultapatient.R;
import com.craterzone.nultapatient.asynctask.RegistrationAsyncTask;
import com.craterzone.nultapatient.model.request.RegistrationReq;
import com.craterzone.nultapatient.util.ConstantUtil;
import com.craterzone.nultapatient.util.NultaSharedPreferenceUtil;

public class RegistrationFragment extends Fragment implements OnClickListener {

	private View mRootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.activity_register, container, false);
		return mRootView;
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
		mRootView.findViewById(R.id.id_camera_img_view).setOnClickListener(this);
		mRootView.findViewById(R.id.request_chat_button).setOnClickListener(this);

	}
	
	private void validation() {
		RegistrationReq registrationReq = new RegistrationReq(); 
		String fullName = ((EditText)mRootView.findViewById(R.id.fullname_edittext)).getText().toString();
		if (fullName.length() == 0) {
			CustomToast.showShortToast(getActivity(), getString(R.string.fullname_validation));
			((EditText)mRootView.findViewById(R.id.fullname_edittext)).requestFocus();
			return;
		}
		registrationReq.setFullName(fullName);
		
		String age = ((EditText)mRootView.findViewById(R.id.age_edittext)).getText().toString();
		if (age.length() == 0) {
			CustomToast.showShortToast(getActivity(), getString(R.string.age_validation));
			((EditText)mRootView.findViewById(R.id.age_edittext)).requestFocus();
			return;
		}
		registrationReq.setAge(Integer.parseInt(age));
		
		
		String gender = ((EditText)mRootView.findViewById(R.id.gender_edittext)).getText().toString();
		if (gender.length() == 0) {
			CustomToast.showShortToast(getActivity(), getString(R.string.gender_validation));
			 ((EditText)mRootView.findViewById(R.id.gender_edittext)).requestFocus();
			 return;
		}
		registrationReq.setGender(gender);
		
		String city = ((EditText)mRootView.findViewById(R.id.city_edittext)).getText().toString();
		if (city.length() == 0) {
			CustomToast.showShortToast(getActivity(), getString(R.string.city_validation));
			((EditText)mRootView.findViewById(R.id.city_edittext)).requestFocus();
			return;
		}
		registrationReq.setCity(111);
		String country = ((EditText)mRootView.findViewById(R.id.country_edittext)).getText().toString();
		if (country.length() == 0) {
			CustomToast.showShortToast(getActivity(), getString(R.string.country_validation));
			((EditText)mRootView.findViewById(R.id.country_edittext)).requestFocus();
			return;
		}
		registrationReq.setPatientId(NultaSharedPreferenceUtil.getInstance(getActivity()).getUserID(0));
		registrationReq.setLres("");
		registrationReq.setHres("");
		registrationReq.setCountry(91);
		new RegistrationAsyncTask(getActivity(),registrationReq).execute();
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			   case R.id.id_camera_img_view:
				      showImageChooserDialog();				
				 break;
			   case R.id.request_chat_button:
				      validation();
				 break;
			}	
	}
	
	
	@Override
		public void onActivityResult(int requestCode, int resultCode, Intent data) {
			Uri uri = null;
			if(resultCode == Activity.RESULT_OK && requestCode == ConstantUtil.IMAGE_ACTIVITY_REQUEST_CODE) {
				uri = data.getData();
			} else if(resultCode == Activity.RESULT_OK && requestCode == ConstantUtil.IMAGE_REQUEST_CODE) {
				uri = CameraUtil.getLastImageUri();
			}
			if(uri != null) {
				Bitmap bitmap = ImageUtil.decodeImageUri(getActivity(), uri);
			    ((ImageView)mRootView.findViewById(R.id.id_doctor_img_view)).setImageBitmap(bitmap);
			}
			
		}
	
	void showImageChooserDialog() {
				final Dialog dialog = new Dialog(getActivity(), R.style.Theme_Dialog);
				dialog.setContentView(R.layout.image_chooser_dialog);
				dialog.findViewById(R.id.id_camera_txt_view).setOnClickListener(new OnClickListener() {
					
				@Override
					public void onClick(View v) {
						CameraUtil.openCamera(getActivity());
						dialog.dismiss();
					}
				});
				dialog.findViewById(R.id.id_gallery_txt_view).setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Intent intent2 = new Intent(Intent.ACTION_GET_CONTENT);
		                intent2.setType("image/*");
		                startActivityForResult(intent2, 200);
		                dialog.dismiss();
					}
				});
				dialog.show();
			}
	
}
