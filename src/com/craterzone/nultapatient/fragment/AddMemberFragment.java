package com.craterzone.nultapatient.fragment;

import com.craterzone.nultapatient.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class AddMemberFragment extends Fragment  {

	View mRootView;
	Spinner mSpinner;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		mRootView = inflater.inflate(R.layout.add_member, container, false);
		mSpinner = (Spinner) mRootView.findViewById(R.id.id_spinner_relation);
		String[] values = getResources().getStringArray(R.array.relation);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				this.getActivity(),
				android.R.layout.simple_spinner_dropdown_item, values);
		mSpinner.setAdapter(adapter);
		
		return mRootView;
	}

	

}
