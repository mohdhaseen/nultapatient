package com.craterzone.nultapatient.enums;

public enum WebService {
	
	VERIFY("verify");
	
	public String url;

	private WebService(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	private static String ROOT_PATH = "http://178.62.225.251:8080";
	private static String PATH_SEPARATOR = "/";
	private static String USER_PATH = "nultaapi/patient/v1";
	
	private static String getRootPath(){
		return ROOT_PATH + PATH_SEPARATOR + USER_PATH;
	}
	
	public static String loginUrl(){
		return getRootPath();
	}
	
	public static String numberVerificationUrl(){
		return getRootPath() + PATH_SEPARATOR + VERIFY.getUrl();
	}
	
	

}
