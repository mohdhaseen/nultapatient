package com.craterzone.nultapatient.util;

public class ConstantUtil {
	
	public static final int IMAGE_REQUEST_CODE = 100;
	public static final int IMAGE_ACTIVITY_REQUEST_CODE = 200;
	public static final String LOGIN_TAG = "login";
	public static final String VERIFY_TAG = "verify";
	public static final String REGISTARTION_TAG = "registartion";
	
}
