package com.craterzone.nultapatient.util;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;


public class NultaSharedPreferenceUtil {

	private final static String TAG = NultaSharedPreferenceUtil.class.getName();
    private static SharedPreferences _pref;
	
	private static NultaSharedPreferenceUtil _instance;
	
	public static final String SHARED_PREF_NAME = "nulta_sp";
	public static final int PRIVATE_MODE = 0;
	
	private enum Keys {
		
		USER_ID("user_id"),
		LATTITUDE("latitude"),
		LONGITUDE("longitude"),
		PHONE_NUMBER("phone_number"),
		COUNTRY_CODE("country_code"),
		IS_USER_REGISTER("is_user_register"),
		IS_USER_LOGIN("is_user_login"),
		IS_USER_VERIFY("is_user_verify");
				
		private String label;
		
		private Keys(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}
	}
	
	private NultaSharedPreferenceUtil() {}
	
	public static NultaSharedPreferenceUtil getInstance(Context context) {
		if(_pref == null) {
			_pref = context.getSharedPreferences(SHARED_PREF_NAME, PRIVATE_MODE);
		}
		if(_instance == null ) {
			_instance = new NultaSharedPreferenceUtil();
		}
		return _instance;
	}
	
	
	/*
	 * this method returns country code
	 * 
	 */
	public String getCountryCode(String defaultValue){
		return getString(Keys.COUNTRY_CODE.getLabel(), defaultValue);
	}
	
	/*
	 * 
	 * this method set country code
	 */
	
	public void setCountryCode(String value){
		setString(Keys.COUNTRY_CODE.getLabel(), value);
	}
	
	
	/*
	 * this method returns phone number
	 * 
	 */
	public String getPhoneNumber(String defaultValue){
		return getString(Keys.PHONE_NUMBER.getLabel(), defaultValue);
	}
	
	/*
	 * 
	 * this method set phone number
	 */
	
	public void setPhoneNumber(String value){
		setString(Keys.PHONE_NUMBER.getLabel(), value);
	}
	
	
	
	/*
	 * this method returns userid
	 * 
	 */
	public long getUserID(long defaultValue){
		return getLong(Keys.USER_ID.getLabel(), defaultValue);
	}
	
	/*
	 * 
	 * this method set user id
	 */
	
	public void setUserID(long value){
		setLong(Keys.USER_ID.getLabel(), value);
	}
	
	/**
	 * This method set lattitude
	 */
	
	
	public void setLattitude(double lat){
		setDouble(Keys.LATTITUDE.getLabel(), lat);
	}
	
	/**
	 * This method return latitude
	 */
	
	public double getLattitude(double defaultValue){
		return getDouble(Keys.LATTITUDE.getLabel(), defaultValue);
	}
	
	/**
	 * This method set longitude
	 */
	public void setLongitude(double lng){
		setDouble(Keys.LONGITUDE.getLabel(), lng);
	}
	
	/**
	 * This method return longitude
	 */
	public  double getLongitude(double defaultValue){
		return getDouble(Keys.LONGITUDE.getLabel(), defaultValue);
	}
	
	/**
	 * This method set user register
	 */
	public void setUserRegister(boolean lng){
		setBoolean(Keys.IS_USER_REGISTER.getLabel(), lng);
	}
	
	/**
	 * This method return user register
	 */
	public  boolean isUserRegister(boolean defaultValue){
		return getBoolean(Keys.IS_USER_REGISTER.getLabel(), defaultValue);
	}

	/**
	 * This method set user register
	 */
	public void setUserLogin(boolean value){
		setBoolean(Keys.IS_USER_LOGIN.getLabel(), value);
	}
	
	/**
	 * This method return user register
	 */
	public  boolean isUserLogin(boolean defaultValue){
		return getBoolean(Keys.IS_USER_LOGIN.getLabel(), defaultValue);
	}
	

	/**
	 * This method set user verify
	 */
	public void setUserVerify(boolean value){
		setBoolean(Keys.IS_USER_VERIFY.getLabel(), value);
	}
	
	/**
	 * This method return user verify
	 */
	public  boolean isUserVerify(boolean defaultValue){
		return getBoolean(Keys.IS_USER_VERIFY.getLabel(), defaultValue);
	}
	
	/**
	 * Set lat n log in shared preference
	 * 
	 * @param context
	 * @param lattitude
	 * @param longitude
	 */
	public static void setLocation(Context context, double lattitude,
			double longitude) {
		try {
			if (_pref == null) {
				_pref = context.getSharedPreferences(SHARED_PREF_NAME, PRIVATE_MODE);
			}
			Editor editor = _pref.edit();
			editor.putString(Keys.LATTITUDE.getLabel(), String.valueOf(lattitude));
			editor.putString(Keys.LONGITUDE.getLabel(), String.valueOf(longitude));
			editor.commit();
		} catch (Exception e) {
			Log.e(TAG,
					"Unable to update lattitude N longitude in Shared preference",
					e);
		}
	}
	

	
	private void setString(String key, String value) {
		if(key != null && value != null ) {
			try {
				if(_pref != null) {
					Editor editor = _pref.edit();
					editor.putString(key, value);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value + "in shared preference", e);
			}
		}
	}
	
	private void setLong(String key, long value) {
		if(key != null) {
			try {
				if(_pref != null) {
					Editor editor = _pref.edit();
					editor.putLong(key, value);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value + "in shared preference", e);
			}
		}
	}
	
	private void setInt(String key, int value) {
		if(key != null) {
			try {
				if(_pref != null) {
					Editor editor = _pref.edit();
					editor.putInt(key, value);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value + "in shared preference", e);
			}
		}
	}
	
	private void setDouble(String key, double value) {
		if(key != null) {
			try {
				if(_pref != null) {
					Editor editor = _pref.edit();
					editor.putFloat(key, (float) value);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value + "in shared preference", e);
			}
		}
	}
	
	private void setBoolean(String key, boolean value) {
		if(key != null) {
			try {
				if(_pref != null) {
					Editor editor = _pref.edit();
					editor.putBoolean(key, value);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value + "in shared preference", e);
			}
		}
	}
	
	private int getInt(String key, int defaultValue) {
		if(_pref != null && key != null && _pref.contains(key)) {
			return _pref.getInt(key, defaultValue);
		}
		return defaultValue;
	}
	
	private long getLong(String key, long defaultValue) {
		if(_pref != null && key != null && _pref.contains(key)) {
			return _pref.getLong(key, defaultValue);
		}
		return defaultValue;
	}
	
	private boolean getBoolean(String key, boolean defaultValue) {
		if(_pref != null && key != null && _pref.contains(key)) {
			return _pref.getBoolean(key, defaultValue);
		}
		return defaultValue;
	}
	
	private String getString(String key, String defaultValue) {
		if(_pref != null && key != null && _pref.contains(key)) {
			return _pref.getString(key, defaultValue);
		}
		return defaultValue;
	}
	private double getDouble(String key, double defaultValue) {
		if(_pref != null && key != null && _pref.contains(key)) {
			return _pref.getFloat(key, (float) defaultValue);
		}
		return defaultValue;
	}
	
	private void removeString(String key) {
		if(key != null) {
			try {
				if(_pref != null && _pref.contains(key)) {
					Editor editor = _pref.edit();
					editor.remove(key);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to remove key" + key, e);
			}
		}
	}
	
	/**
	 * This Method Clear shared preference.
	 */
	public void clear() {
		Editor editor = _pref.edit();
		editor.clear();
		editor.commit();
	}
	
}
