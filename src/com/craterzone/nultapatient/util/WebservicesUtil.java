package com.craterzone.nultapatient.util;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.cz_commons_lib.Logger;
import com.craterzone.httpclient.HttpUrlConnectionUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.craterzone.nultapatient.enums.WebService;
import com.craterzone.nultapatient.model.request.LoginReq;
import com.craterzone.nultapatient.model.request.RegistrationReq;
import com.craterzone.nultapatient.model.request.VerificationReq;

public class WebservicesUtil {
	
public static final String TAG = WebservicesUtil.class.getName();
	
	public static final String APPLICATION_JSON = "application/json";
	public static final String CONTENT_TYPE = APPLICATION_JSON;
	public static final String ACCEPT_TYPE = APPLICATION_JSON;
	
	/**
	 * Login Patient
	 * @param loginReq
	 * @return
	 */
	public static CZResponse loginPatient(LoginReq loginReq) {
		try {
			if(loginReq != null){
			   CZResponse response = HttpUrlConnectionUtil.post(WebService.loginUrl(), JsonUtil.toJson(loginReq), CONTENT_TYPE, ACCEPT_TYPE, null);
			    return response;
			}
		} catch (Exception e) {
			Logger.e(TAG, "Error in login User", e);
		}
		return null;
	}
	
	/**
	 * Verify Phone Number
	 * @param verificationRequest
	 * @return
	 */
	public static CZResponse verifyPhoneNumber(VerificationReq verificationRequest){
		 try {
				if(verificationRequest != null){
				   CZResponse response =HttpUrlConnectionUtil.post(WebService.numberVerificationUrl(), JsonUtil.toJson(verificationRequest), CONTENT_TYPE, ACCEPT_TYPE, null);
				    return response;
				}
			} catch (Exception e) {
				Logger.e(TAG, "Error in  verify phone number", e);
			}
			return null;
	}
	
	/**
	 * Login Patient
	 * @param loginReq
	 * @return
	 */
	public static CZResponse registerPatient(RegistrationReq registerReq) {
		try {
			if(registerReq != null){
			   CZResponse response = HttpUrlConnectionUtil.put(WebService.loginUrl(), JsonUtil.toJson(registerReq), CONTENT_TYPE, ACCEPT_TYPE, null);
			    return response;
			}
		} catch (Exception e) {
			Logger.e(TAG, "Error in registring User", e);
		}
		return null;
	}

}
