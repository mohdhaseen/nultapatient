package com.craterzone.nultapatient.fragmentdialog;

import com.craterzone.nultapatient.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class PhotoDescDialog extends DialogFragment implements OnClickListener {
	private	ImageView mImageView;
	private	Button mButton;
	private EditText mEditText;
	private TextView mTextView;
	View mRootView;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		mRootView=(View)inflater.inflate(R.layout.image_description_dialog,
				null);
		builder.setView(mRootView);
mImageView=(ImageView)mRootView.findViewById(R.id.id_close_btn_image);
mImageView.setOnClickListener(this);
mButton=(Button)mRootView.findViewById(R.id.id_next_btn);
mButton.setOnClickListener(this);
mEditText=(EditText)mRootView.findViewById(R.id.id_brief_desc);
mTextView=(TextView)mRootView.findViewById(R.id.remaining_chars);
mEditText.addTextChangedListener(new TextWatcher() {
	
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		
		
	}
	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		
		
		
	}
	
	@Override
	public void afterTextChanged(Editable s) {
		
		String str=String.format(getString(R.string.chars),String.valueOf(s.length()) );
		mTextView.setText(str);
	}
});
		return builder.create();

	}

	@Override
	public void onClick(View v) {
		if(v.getId()==R.id.id_close_btn_image)
		this.dismiss();
		else if(v.getId()==R.id.id_next_btn)
			this.dismiss();
		
			
		
	}

}
