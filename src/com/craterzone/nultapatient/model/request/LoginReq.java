package com.craterzone.nultapatient.model.request;

public class LoginReq {
	
	private String mobile;
	private int country;


	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getCountry() {
		return country;
	}

	public void setCountry(int country) {
		this.country = country;
	}

}
