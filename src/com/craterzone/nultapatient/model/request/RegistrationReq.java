package com.craterzone.nultapatient.model.request;

public class RegistrationReq {
	private long patientId;
	private String fullName;
	private String gender = "MALE";
	private int age;
	private int city;
	private int country;
	private String chatLanguage = "ENGLISH";
	private String hres ;
	private String lres;
	public long getPatientId() {
		return patientId;
	}
	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getCity() {
		return city;
	}
	public void setCity(int city) {
		this.city = city;
	}
	public int getCountry() {
		return country;
	}
	public void setCountry(int country) {
		this.country = country;
	}
	public String getChatLanguage() {
		return chatLanguage;
	}
	public void setChatLanguage(String chatLanguage) {
		this.chatLanguage = chatLanguage;
	}
	public String getHres() {
		return hres;
	}
	public void setHres(String hres) {
		this.hres = hres;
	}
	public String getLres() {
		return lres;
	}
	public void setLres(String lres) {
		this.lres = lres;
	}
	
	
}
