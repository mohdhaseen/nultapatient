package com.craterzone.nultapatient.model.response;

public class LoginResponse {

	private long userId;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
}
