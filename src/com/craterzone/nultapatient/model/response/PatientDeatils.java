package com.craterzone.nultapatient.model.response;

public class PatientDeatils {
	
	private long patientId;
	private String fullName;
	private String gender;
	private int age;
	private String relationShip;
	public long getPatientId() {
		return patientId;
	}
	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getRelationShip() {
		return relationShip;
	}
	public void setRelationShip(String relationShip) {
		this.relationShip = relationShip;
	}
	
	//"patientId":1427463864852,"fullName":"rohan","gender":"MALE","age":23,"relationShip":"BROTHER"
}
