package com.craterzone.nultapatient.model.response;

import java.util.ArrayList;

public class VerificationResponse {
	private long patientId;
	private String fullName;
	private String gender;
	private int age;
	private String hres;
	private String lres;
	private String country;
	private String city;
	private String mobileNo;
	private ArrayList<PatientDeatils> memberList;
	private double balance;
	private String balanceType;
	private String chatLanguge;
	private boolean exist;

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getHres() {
		return hres;
	}

	public void setHres(String hres) {
		this.hres = hres;
	}

	public String getLres() {
		return lres;
	}

	public void setLres(String lres) {
		this.lres = lres;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public ArrayList<PatientDeatils> getMemberList() {
		return memberList;
	}

	public void setMemberList(ArrayList<PatientDeatils> memberList) {
		this.memberList = memberList;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getBalanceType() {
		return balanceType;
	}

	public void setBalanceType(String balanceType) {
		this.balanceType = balanceType;
	}

	public String getChatLanguge() {
		return chatLanguge;
	}

	public void setChatLanguge(String chatLanguge) {
		this.chatLanguge = chatLanguge;
	}

	public boolean isExist() {
		return exist;
	}

	public void setExist(boolean exist) {
		this.exist = exist;
	}

	
}