package com.craterzone.nultapatient.asynctask;

import org.apache.http.HttpStatus;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.craterzone.httpclient.model.CZResponse;
import com.craterzone.nultapatient.R;
import com.craterzone.nultapatient.activity.ChatsActivity;
import com.craterzone.nultapatient.model.request.RegistrationReq;
import com.craterzone.nultapatient.util.WebservicesUtil;

public class RegistrationAsyncTask extends AsyncTask<String, String, Integer>{
	
    private static final String TAG = RegistrationAsyncTask.class.getSimpleName();
	
	Context mContext;
	RegistrationReq mRegistartionReq;
	CZResponse mResponse;
	ProgressDialog mDialog;

	public RegistrationAsyncTask(Context context, RegistrationReq registartionReq) {
		this.mContext = context;
		this.mRegistartionReq = registartionReq; 
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		try {
			mDialog = ProgressDialog.show(mContext, mContext.getResources().getString(R.string.title), mContext.getResources().getString(R.string.message));
		} catch (Exception e) {
		}
	}
	
	@Override
	protected Integer doInBackground(String... params) {
		int status = 0;
		try {
			mResponse = WebservicesUtil.registerPatient(mRegistartionReq);
			if(mResponse == null){
				return status;
			}
			status = mResponse.getResponseCode();
			if (status == HttpStatus.SC_OK) {

			}
			return status;
		} catch (Exception e) {
			Log.e(TAG,"Error in register patient ");
		}

		return status;
	}


	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);
		if(mDialog!=null && mDialog.isShowing())
		{
			mDialog.dismiss();
		}
		if(result == HttpStatus.SC_OK ){
			Intent intent = new Intent(mContext, ChatsActivity.class);
			mContext.startActivity(intent);
		}
		
	}

}
