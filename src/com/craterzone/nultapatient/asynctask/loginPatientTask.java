package com.craterzone.nultapatient.asynctask;

import org.apache.http.HttpStatus;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.cz_commons_lib.Logger;
import com.craterzone.cz_view_lib.CustomToast;
import com.craterzone.httpclient.model.CZResponse;
import com.craterzone.nultapatient.R;
import com.craterzone.nultapatient.fragment.PhoneNumVerificationFragment;
import com.craterzone.nultapatient.model.request.LoginReq;
import com.craterzone.nultapatient.model.response.LoginResponse;
import com.craterzone.nultapatient.util.ConstantUtil;
import com.craterzone.nultapatient.util.NultaSharedPreferenceUtil;
import com.craterzone.nultapatient.util.WebservicesUtil;

public class loginPatientTask extends AsyncTask<String, String, Integer> {
	
	public static String TAG = loginPatientTask.class.getName();
	
	private Context mContext;
	private LoginReq mLogin;
	private ProgressDialog mDialog;

	public loginPatientTask(Context mContext,LoginReq loginReq) {
		super();
		this.mContext = mContext;
		this.mLogin = loginReq;
	}
	
	@Override
	protected void onPreExecute() {
		try {
			mDialog = ProgressDialog.show(mContext,"",mContext.getResources().getString(R.string.message));
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
		
		super.onPreExecute();
	}

	
	@Override
	protected  Integer doInBackground(String... params) {
		LoginResponse loginResponse = new LoginResponse();
		int statusCode = 0;
		try {
			CZResponse response = WebservicesUtil.loginPatient(mLogin);
			if(response == null){
				return 0;
			}
			statusCode  = response.getResponseCode();
			if (statusCode == HttpStatus.SC_OK) {
				loginResponse = (LoginResponse)JsonUtil.toModel(response.getResponseString(), LoginResponse.class);
	            NultaSharedPreferenceUtil.getInstance(mContext).setUserID(loginResponse.getUserId());
	            NultaSharedPreferenceUtil.getInstance(mContext).setCountryCode(Integer.toString(mLogin.getCountry()));
	            NultaSharedPreferenceUtil.getInstance(mContext).setPhoneNumber(mLogin.getMobile());
			}
			return statusCode;
		} catch (Exception e) {
			Logger.e(TAG, "Error in getting login response " + e.getMessage());
		}
		return statusCode;
	}
	
	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);
		if (mDialog!=null  && mDialog.isShowing()) {
			mDialog.dismiss();
			
		}
		if(result == HttpStatus.SC_OK){
			((FragmentActivity)mContext).getFragmentManager().beginTransaction().replace(R.id.fragment_container, 
					new PhoneNumVerificationFragment(), ConstantUtil.VERIFY_TAG).addToBackStack(ConstantUtil.LOGIN_TAG).commit();
			return;
		}else if(result == HttpStatus.SC_CONFLICT)
		{
			CustomToast.showShortToast(mContext, mContext.getResources().getString(R.string.already_registered));
			return;
		}else{
			CustomToast.showShortToast(mContext, mContext.getResources().getString(R.string.error_in_login));
		}
		

	}

}
