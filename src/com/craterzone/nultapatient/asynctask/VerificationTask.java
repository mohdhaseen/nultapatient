package com.craterzone.nultapatient.asynctask;

import org.apache.http.HttpStatus;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.craterzone.cz_commons_lib.Logger;
import com.craterzone.cz_view_lib.CustomToast;
import com.craterzone.httpclient.model.CZResponse;
import com.craterzone.nultapatient.R;
import com.craterzone.nultapatient.fragment.RegistrationFragment;
import com.craterzone.nultapatient.model.request.VerificationReq;
import com.craterzone.nultapatient.util.ConstantUtil;
import com.craterzone.nultapatient.util.WebservicesUtil;

public class VerificationTask extends AsyncTask<String, String, Integer>{

	public static String TAG = VerificationTask.class.getName();
	
	private Context mContext;
	private VerificationReq mRequestModel;
	private ProgressDialog mDialog;
	
	public VerificationTask(Context context,VerificationReq request) {
		this.mContext =context;
		this.mRequestModel=request;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		try {
			mDialog = ProgressDialog.show(mContext, "", mContext.getResources().getString(R.string.message));
		} catch (Exception e) {
		}
	}
	
	@Override
	protected Integer doInBackground(String... params) {
		try {
		   CZResponse response = WebservicesUtil.verifyPhoneNumber(mRequestModel);
		  return response.getResponseCode();
		} catch (Exception e) {
			Logger.e(TAG, "Error in verify patient" + e.getMessage());
		}
		return 0;
	}


	@Override
	protected void onPostExecute(Integer result) {
		try {
			if (mDialog!=null &&  mDialog.isShowing()) {
				mDialog.dismiss();
			} 
			if(result == HttpStatus.SC_OK){
				FragmentManager fragmentManager = ((FragmentActivity)mContext).getFragmentManager();
				fragmentManager.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
				fragmentManager.beginTransaction().replace(R.id.fragment_container, 
						new RegistrationFragment(), ConstantUtil.REGISTARTION_TAG).commit();
			}else{
				CustomToast.showShortToast(mContext, "Error in verify phone number ,please try again!!!");
		 	}
		} catch (Exception e) {
			Log.e(TAG, "Error in verify phone number.");
		}
		super.onPostExecute(result);
	}
	
	
	

}
