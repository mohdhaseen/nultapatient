package com.craterzone.nultapatient.activity;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Window;

import com.craterzone.nultapatient.R;
import com.craterzone.nultapatient.fragment.AddMemberFragment;
import com.craterzone.nultapatient.fragment.LoginFragment;
import com.craterzone.nultapatient.util.ConstantUtil;
import com.craterzone.nultapatient.util.NultaSharedPreferenceUtil;

public class LoginActivity extends FragmentActivity {
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_login);
		boolean isUserVerify = NultaSharedPreferenceUtil.getInstance(this).isUserVerify(false);
		boolean isUserRegister = NultaSharedPreferenceUtil.getInstance(this).isUserRegister(false);
		if(isUserVerify){
		   /*getFragmentManager().beginTransaction().add(R.id.fragment_container, new LoginFragment(),ConstantUtil.LOGIN_TAG)
		    .commit();*/
			getFragmentManager().beginTransaction().add(R.id.fragment_container, new AddMemberFragment(),ConstantUtil.LOGIN_TAG)
		    .commit();
		}else{
			 getFragmentManager().beginTransaction().add(R.id.fragment_container, new AddMemberFragment(),ConstantUtil.LOGIN_TAG)
			    .commit();
		}
	}
	
	
	@Override
	public void onBackPressed() {
		    if (getFragmentManager().getBackStackEntryCount() > 0 ){
		        getFragmentManager().popBackStack();
		    } else {
		        super.onBackPressed();
		    }
		}
	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Fragment fragment = getFragmentManager().findFragmentByTag(ConstantUtil.REGISTARTION_TAG);
		fragment.onActivityResult(requestCode, resultCode, data);
		
	}
	

}
