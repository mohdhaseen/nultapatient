package com.craterzone.nultapatient.activity;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import com.craterzone.nultapatient.R;
import com.craterzone.nultapatient.fragmentdialog.PhotoDescDialog;

public class ChatRequestActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.chat_request);
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		findViewById(R.id.available_doctor_button).setOnClickListener(this);
		
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.available_doctor_button:
			//Intent intent = new Intent(ChatRequestActivity.this,LoginActivity.class);
			Intent intent=new Intent(ChatRequestActivity.this,ChatsActivity.class);
			startActivity(intent);
			/*DialogFragment newFragment = new PhotoDescDialog();
		    newFragment.show(getFragmentManager(), "PhotoDesc");*/
			break;

		default:
			break;
		}
	}
	
}
