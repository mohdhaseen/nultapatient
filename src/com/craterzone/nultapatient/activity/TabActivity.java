package com.craterzone.nultapatient.activity;

import com.craterzone.nultapatient.R;
import com.craterzone.nultapatient.fragment.NultaFriendsFragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class TabActivity extends FragmentActivity {
	
	private static final String TAG = TabActivity.class.getName();
	private static final String TAG_CHAT = "tag_chat";
	private static final String TAG_NULTA_FRIENDS = "tag_nulta_friends";
	private static final String TAG_PAYMENT = "tag_payment";
	private FragmentTabHost mTabHost;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_tab);
		addTabs();
	}


	private void addTabs() {
		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.frame_container);
		TabSpec spec = null;
		
		// chatting tab
		spec = mTabHost.newTabSpec(TAG_CHAT).setIndicator(getView(R.drawable.tab_item_chat_img_selector, R.string.chat));
		mTabHost.addTab(spec, NultaFriendsFragment.class, null);
		
		// suggest doctor tab
		//Intent intent = new Intent(this, SuggestedDocActivity.class);
		spec = mTabHost.newTabSpec(TAG_NULTA_FRIENDS).setIndicator(getView(R.drawable.tab_item_suggest_doc_img_selector, R.string.nulta_friends));
		mTabHost.addTab(spec, NultaFriendsFragment.class, null);

		// patient list
		spec = mTabHost.newTabSpec(TAG_PAYMENT).setIndicator(getView(R.drawable.tab_item_patient_list_img_selector, R.string.payments));
		mTabHost.addTab(spec, NultaFriendsFragment.class, null);

		/*mTabHost.setOnTabChangedListener(new OnTabChangeListener() {
			
			@Override
			public void onTabChanged(String tabId) {
				if(tabId == TAG_SUGGEST_DOC){
				//	Intent intent = new Intent(TabActivity.this, SuggestedDocActivity.class);
					//startActivity(intent);
				}
			}
		});*/
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if(getIntent().hasExtra("selectedTab")){
			mTabHost.setCurrentTab(getIntent().getIntExtra("selectedTab", 0));
		}
	}

	

	private View getView(int drawbleId, int textId) {
		View view = LayoutInflater.from(this).inflate(R.layout.tabs, null);
		TextView text = (TextView)view.findViewById(R.id.id_tabs_indicator_txt_view);
		ImageView img = (ImageView) view.findViewById(R.id.id_tab_item_img_view);
		text.setText(getResources().getString(textId));
		img.setImageResource(drawbleId);
		Log.d(TAG_CHAT, "created view for tab item : " +getResources().getString(textId));
		return view;
	}
	
	public static Intent createPatientListIntent(Context context, int selectedTab){
		Intent intent = new Intent(context, TabActivity.class);
		intent.putExtra("selectedTab", selectedTab);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
		return intent;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		     mTabHost.setCurrentTab(0);
	}
	
	public void onBackPress(){
		mTabHost.setCurrentTab(0);
	}
	
	@Override
	public void onBackPressed() {
		if(mTabHost.getCurrentTab() == 0){
			super.onBackPressed();
		}else{
			mTabHost.setCurrentTab(0);
		}
	}

}
